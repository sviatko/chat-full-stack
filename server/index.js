const express = require("express");
const cors = require("cors");
const { PORT } = require("./config/app");
const { responseMiddleware } = require("./middlewares/response.middleware");
require("./config/db");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require("./routes/index");
routes(app);

app.use(responseMiddleware);

app.listen(PORT, () => {
  console.log(`Listen on ${PORT}`);
});

exports.app = app;
