const { Router } = require("express");

const { MessageRepository } = require("../repositories/messageRepository");

const router = Router();

router.get("/", (req, res, next) => {
  res.data = MessageRepository.getAll();
  next();
});

router.post("/", (req, res, next) => {
  const data = { ...req.body };
  data.createdAt = new Date().toISOString();
  const message = MessageRepository.create(data);
  res.data = message;

  next();
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  const message = MessageRepository.getOne({ id });

  res.data = message;

  next();
});

router.put("/:id", (req, res, next) => {
  const id = req.params.id;
  console.log(req.body);
  const message = MessageRepository.update(id, req.body);
  console.log(req.body);

  res.data = message;

  next();
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  MessageRepository.delete(id);

  next();
});

module.exports = router;
