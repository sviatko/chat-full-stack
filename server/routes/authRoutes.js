const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post("/login", (req, res, next) => {
  const body = req.body;
  // TODO: Implement login action (get the user if it exist with entered credentials)
  res.data = AuthService.login({
    email: body.email,
    password: body.password,
  });

  next();
});

module.exports = router;
