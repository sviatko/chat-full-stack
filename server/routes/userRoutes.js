const { Router } = require("express");
const UserService = require("../services/userService");
const { UserRepository } = require("../repositories/userRepository");
const {
  createUserValid,
  updateUserValid,
  checkUserExists,
  checkUsersExist,
} = require("../middlewares/user.validation.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.get("/", (request, response, next) => {
  response.data = UserRepository.getAll();

  next();
});

router.get("/:id", (request, response, next) => {
  const id = request.params.id;

  response.data = UserRepository.getOne({ id });

  next();
});

router.post("/", (request, response, next) => {
  const userData = { ...request.body };
  userData.role = "user";
  response.data = UserRepository.create(userData);
  console.log(userData);
  next();
});

router.put("/:id", (request, response, next) => {
  const userId = request.params.id;
  const userData = request.body;

  response.data = UserRepository.update(userId, userData);
  next();
});

router.delete("/:id", (request, response, next) => {
  UserRepository.delete(request.params.id);

  response.data = {
    error: false,
    message: "User deleted successfuly",
  };

  next();
});

module.exports = router;
