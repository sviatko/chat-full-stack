import React from "react";

import Chat from "../../components/Chat";
import useStyles from "../../assets/jss/components/appStyle";

const Messages = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Chat />
    </div>
  );
};

export default Messages;
