import React, { useEffect } from "react";
import { Container, Grid } from "@material-ui/core";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import * as actionCreator from "../../store/actions";
import Table from "../../components/Table";
import Button from "../../components/Button";
import Loader from "../../components/Loader";
import { IUser } from "../../interfaces/IUser";
import { IUserState } from "../../interfaces/IUserState";
import history from "../../routes/history";
import { Paths } from "../../routes/paths";
import useStyles from "../../assets/jss/sharedStyle";

type Props = {
  isFetching: boolean;
  users: Array<IUser>;
  fetchUsers: () => void;
  deleteUser: (id: string) => void;
};
const Users = ({ users, isFetching, fetchUsers, deleteUser }: Props) => {
  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  const classes = useStyles();
  const onEdit = (id: string) => history.push(`${Paths.USERS}/${id}`);
  const onDelete = (id: string) => {
    if (window.confirm("Are you sure, you want to delete the user?"))
      deleteUser(id);
  };

  const onCreate = () => history.push(Paths.USERS_CREATE);

  return (
    <Container maxWidth={"md"}>
      <Grid container>
        <Grid item md={12}>
          <Button title={"Add user"} onClick={onCreate} type={"outlined"} />
        </Grid>
        <Grid item md={12} className={classes.relative}>
          {isFetching && <Loader />}
          <Table rows={users} onEdit={onEdit} onDelete={onDelete} />
        </Grid>
      </Grid>
    </Container>
  );
};

const mapStateToProps = (state: { user: IUserState }) => {
  return {
    users: state.user.users,
    isFetching: state.user.isUsersFetching,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchUsers: () => dispatch(actionCreator.fetchUsers()),
    deleteUser: (id: string) => dispatch(actionCreator.deleteUser(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
