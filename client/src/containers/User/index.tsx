import React, { useEffect } from "react";
import { Container, Card, CardContent, Grid } from "@material-ui/core";
import { useParams, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import InputField from "../../components/InputField";
import Button from "../../components/Button";
import useStyles from "../../assets/jss/containers/userStyle";
import sharedStyle from "../../assets/jss/sharedStyle";
import * as actionCreator from "../../store/actions";
import { Paths } from "../../routes/paths";
import { IUser } from "../../interfaces/IUser";
import { IUserState } from "../../interfaces/IUserState";
import Loader from "../../components/Loader";

type Props = {
  isFetching: boolean;
  user: IUser | null;
  fetchUser: (id: string) => void;
  editCurrentUser: (value: string, field: string) => void;
  updateCurrentUser: (user: IUser) => void;
  createCurrentUser: (user: IUser) => void;
};
const User = ({
  user,
  isFetching,
  fetchUser,
  editCurrentUser,
  createCurrentUser,
  updateCurrentUser,
}: Props) => {
  const sharedClasses = sharedStyle();
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();

  useEffect(() => {
    fetchUser(id);
  }, [fetchUser, id]);

  const backToUsers = () => history.push(Paths.USERS);

  let onSave = () => {};

  if (user) {
    if (user.id) {
      onSave = () => updateCurrentUser(user);
    } else {
      onSave = () => createCurrentUser(user);
    }
  }

  return (
    <Container maxWidth={"md"}>
      <Card>
        <CardContent>
          <Grid container>
            <Grid item md={12}>
              <h3>Edit message</h3>
            </Grid>
            <Grid item md={12} className={sharedClasses.relative}>
              {isFetching && <Loader />}
            </Grid>
            <Grid item sm={12} md={4} className={classes.gridPadding}>
              <InputField
                id={"username"}
                label={"Type your username"}
                value={user ? user.username : ""}
                rows={1}
                onChange={(e) => {
                  editCurrentUser(e.target.value, "username");
                }}
              />
            </Grid>
            <Grid item sm={12} md={4} className={classes.gridPadding}>
              <InputField
                id={"email"}
                label={"Type your email"}
                value={user ? user.email : ""}
                rows={1}
                onChange={(e) => {
                  editCurrentUser(e.target.value, "email");
                }}
              />
            </Grid>
            <Grid item sm={12} md={4} className={classes.gridPadding}>
              <InputField
                id={"password"}
                label={"Type your password"}
                value={user ? user.password : ""}
                rows={1}
                onChange={(e) => {
                  editCurrentUser(e.target.value, "password");
                }}
              />
            </Grid>
            <Grid item md={10}></Grid>
            <Grid item md={2}>
              <div className={classes.root}>
                <Button type={"text"} title={"Cancel"} onClick={backToUsers} />
                <Button type={"outlined"} title={"Send"} onClick={onSave} />
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  );
};

const mapStateToProps = (state: { user: IUserState }) => {
  return {
    user: state.user.currentUser,
    isFetching: state.user.isUsersFetching,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchUser: (id: string) => dispatch(actionCreator.fetchUserById(id)),
    editCurrentUser: (value: string, field: string) =>
      dispatch(actionCreator.editCurrentUser(value, field)),
    updateCurrentUser: (user: IUser) =>
      dispatch(actionCreator.updateCurrentUser(user)),
    createCurrentUser: (user: IUser) =>
      dispatch(actionCreator.createUser(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
