import React, { useEffect } from "react";
import { Container, Card, CardContent, Grid } from "@material-ui/core";
import { useParams, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import InputField from "../../components/InputField";
import Button from "../../components/Button";
import useStyles from "../../assets/jss/containers/messageStyle";
import { IChatState } from "../../interfaces/IChatState";
import * as actionCreator from "../../store/actions";
import { Paths } from "../../routes/paths";
import { IMessage } from "../../interfaces/IMessage";

type Props = {
  currentMessage: string;
  editingMessage: IMessage | null;
  fetchMessageById: (id: string) => void;
  updateMessage: (message: IMessage) => void;
  setCurrentMessage: (message: string) => void;
};
const Message = ({
  currentMessage,
  editingMessage,
  fetchMessageById,
  setCurrentMessage,
  updateMessage,
}: Props) => {
  const classes = useStyles();
  const { id } = useParams();
  const history = useHistory();

  useEffect(() => {
    fetchMessageById(id);
  }, [id, fetchMessageById]);

  const backToMessages = () => history.push(Paths.MESSAGES);
  const submitMessage = editingMessage
    ? () => updateMessage({ ...editingMessage, text: currentMessage })
    : () => {};

  return (
    <Container maxWidth={"md"}>
      <Card>
        <CardContent>
          <Grid container>
            <Grid item md={12}>
              <h3>Edit message</h3>
            </Grid>
            <Grid item md={12}>
              <InputField
                id={"message"}
                label={"Type your message"}
                value={currentMessage}
                rows={3}
                onChange={(e) => {
                  setCurrentMessage(e.target.value);
                }}
              />
            </Grid>
            <Grid item md={10}></Grid>
            <Grid item md={2}>
              <div className={classes.root}>
                <Button
                  type={"text"}
                  title={"Cancel"}
                  onClick={backToMessages}
                />
                <Button
                  type={"outlined"}
                  title={"Send"}
                  onClick={submitMessage}
                />
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    currentMessage: state.chat.currentMessage,
    editingMessage: state.chat.editingMessage,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchMessageById: (id: string) =>
      dispatch(actionCreator.fetchMessageById(id)),
    updateMessage: (message: IMessage) =>
      dispatch(actionCreator.updateMessage(message)),
    setCurrentMessage: (message: string) =>
      dispatch(actionCreator.setCurrentMessage(message)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Message);
