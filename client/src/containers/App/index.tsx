import React from "react";
import { connect } from "react-redux";

import Logo from "../../components/Logo";
import Footer from "../../components/Footer";
import useStyles from "../../assets/jss/components/appStyle";
import Routes from "../../routes";
import { IAuthState } from "../../interfaces/IAuthState";
import history from "../../routes/history";
import { Paths } from "../../routes/paths";

const App = ({ role }: { role: string }) => {
  const classes = useStyles();

  if (role === "") {
    history.push(Paths.HOME);
  }

  return (
    <div className={classes.root}>
      <div>
        <Logo />
        <Routes />
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state: { auth: IAuthState }) => {
  return {
    role: state.auth.data.role,
  };
};

export default connect(mapStateToProps)(App);
