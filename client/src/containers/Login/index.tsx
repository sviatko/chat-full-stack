import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Grid, Container, Card, CardContent } from "@material-ui/core";

import useStyles from "../../assets/jss/containers/loginStyle";
import sharedStyle from "../../assets/jss/sharedStyle";
import InputField from "../../components/InputField";
import Button from "../../components/Button";
import { IAuthState } from "../../interfaces/IAuthState";
import { authChange, auth } from "../../store/actions";

type Props = {
  error: string | null;
  email: string;
  password: string;
  onChange: (field: string, value: string) => void;
  auth: (email: string, password: string) => void;
};
const Login = ({ error, email, password, onChange, auth }: Props) => {
  const classes = useStyles();
  const sharedClasses = sharedStyle();

  return (
    <div className={classes.root}>
      <Container maxWidth={"xs"}>
        <Card>
          <CardContent>
            <h3>Login</h3>
            {error && <span className={sharedClasses.error}>{error}</span>}
            <Grid container>
              <Grid item md={12}>
                <InputField
                  inputProps={{
                    type: "email",
                  }}
                  label={"Email"}
                  id={"email"}
                  value={email}
                  rows={1}
                  onChange={(e) => onChange("email", e.target.value)}
                />
              </Grid>
              <Grid item md={12} className={sharedClasses.mt}>
                <InputField
                  label={"Password"}
                  id={"password"}
                  inputProps={{
                    type: "password",
                  }}
                  value={password}
                  rows={1}
                  onChange={(e) => onChange("password", e.target.value)}
                />
              </Grid>
              <Grid item md={12} className={sharedClasses.mt}>
                <Button
                  title={"Login"}
                  type={"outlined"}
                  onClick={() => auth(email, password)}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Container>
    </div>
  );
};

const mapStateToProps = (state: { auth: IAuthState }) => {
  return {
    email: state.auth.data.email,
    password: state.auth.data.password,
    error: state.auth.error,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onChange: (field: string, value: string) =>
      dispatch(authChange(field, value)),
    auth: (email: string, password: string) => dispatch(auth(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
