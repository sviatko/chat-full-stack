import React from "react";

import useStyles from "../../assets/jss/components/dividerStyle";

type Props = { text: string };

function Divider({ text }: Props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <span className={classes.text}>{text}</span>
    </div>
  );
}

export default Divider;
