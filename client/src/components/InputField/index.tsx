import React, { ChangeEvent } from "react";
import { FormControl, InputLabel, Input } from "@material-ui/core";

import useStyles from "../../assets/jss/components/inputFieldStyle";

type Props = {
  label: string;
  id: string;
  value: string;
  rows?: number;
  inputProps?: any;
  onChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
};

const InputField = ({
  inputProps,
  label,
  id,
  value,
  rows,
  onChange,
}: Props) => {
  const classes = useStyles();

  return (
    <FormControl fullWidth>
      <InputLabel
        variant={"outlined"}
        htmlFor={id}
        classes={{ focused: classes.focused }}
      >
        {label}
      </InputLabel>
      <Input
        id={id}
        fullWidth
        autoComplete={"off"}
        value={value}
        onChange={onChange}
        inputProps={{ ...inputProps }}
        rows={rows || 1}
        multiline={inputProps && inputProps.type !== "password"}
        classes={{
          underline: classes.underline,
        }}
      />
    </FormControl>
  );
};

export default InputField;
