import React from "react";
import { Button } from "@material-ui/core";

import useStyles from "../../assets/jss/components/buttonStyle";

type Props = {
  title: string;
  type: "outlined" | "text";
  color?: "primary" | "secondary";
  onClick: () => void;
};

const CustomButton = ({ title, type, onClick, color = "primary" }: Props) => {
  const classes = useStyles();
  return (
    <Button
      variant={type}
      color={color}
      onClick={onClick}
      classes={{
        root: classes.root,
        outlinedPrimary: classes.outlinedPrimary,
        textPrimary: classes.textPrimary,
        textSecondary: classes.textSecondary,
      }}
    >
      {title}
    </Button>
  );
};

export default CustomButton;
