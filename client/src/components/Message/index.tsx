import React, { useState } from "react";
import PropTypes from "prop-types";
import { Paper, Avatar, Tooltip } from "@material-ui/core";

import useStyles from "../../assets/jss/components/messageStyle";
import Like from "../Like";
import Edit from "../Edit";
import { IMessage } from "../../interfaces/IMessage";

type Props = {
  text: string;
  avatartUrl: string;
  time: string;
  tooltip: string;
  floatRight: boolean;
  onClick: (message: IMessage) => void;
  removeMessageHandler: () => void;
};

const Message = ({
  text,
  avatartUrl,
  time,
  tooltip,
  floatRight,
  onClick,
  removeMessageHandler,
}: Props) => {
  const classes = useStyles();
  const [isLiked, setIsLiked] = useState<Boolean>(false);
  const [isHovered, setIsHovered] = useState<Boolean>(false);

  const giveLikeHandler = () => {
    setIsLiked(!isLiked);
  };

  const doubleClickHandler = floatRight
    ? removeMessageHandler
    : giveLikeHandler;

  return (
    <div className={floatRight ? classes.right : ""}>
      <Paper
        className={classes.root}
        elevation={0}
        onDoubleClick={doubleClickHandler}
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <Tooltip title={tooltip}>
          <div>
            {!floatRight && (
              <Avatar
                className={classes.avatar}
                alt="avatar"
                src={avatartUrl}
              />
            )}
            <p className={classes.message}>{text}</p>
            <span className={classes.time}>{time}</span>
            {isLiked && <Like />}
            {isHovered && floatRight && <Edit onClick={onClick} />}
          </div>
        </Tooltip>
      </Paper>
    </div>
  );
};

export default Message;

Message.prototype = {
  text: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  floatRight: PropTypes.bool,
  onClick: PropTypes.func,
  removeMessageHander: PropTypes.func,
};
