import React from "react";
import { CircularProgress } from "@material-ui/core";

import loaderStyles from "../../assets/jss/components/loaderStyle";

const Loader = () => {
  const classes = loaderStyles();
  return <CircularProgress className={classes.root} />;
};

export default Loader;
