import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
} from "@material-ui/core/";

import { IUser } from "../../interfaces/IUser";
import Button from "../Button";

type Props = {
  rows: Array<IUser>;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
};

const PaginationActionsTable = ({ rows, onEdit, onDelete }: Props) => {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="custom pagination table">
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.username}>
              <TableCell component="th" scope="row">
                {row.username}
              </TableCell>
              <TableCell align="center">{row.email}</TableCell>
              <TableCell align="right">
                <Button
                  title={"delete"}
                  color={"secondary"}
                  type={"text"}
                  onClick={() => onDelete(row.id)}
                />
                <Button
                  title={"edit"}
                  type={"outlined"}
                  onClick={() => onEdit(row.id)}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PaginationActionsTable;
