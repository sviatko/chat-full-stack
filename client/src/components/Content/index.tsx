import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { Card, CardContent, CircularProgress } from "@material-ui/core";
import { Dispatch } from "redux";
import moment from "moment";

import useStyles from "../../assets/jss/components/contentStyle";
import Message from "../Message";
import Divider from "../Divider";
import { IChatState } from "../../interfaces/IChatState";
import { IMessage } from "../../interfaces/IMessage";
import { getBensMessage, groupByDay } from "../../services/MessageService";
import * as actionCreator from "../../store/actions";
import history from "../../routes/history";
import { Paths } from "../../routes/paths";

type Props = {
  isChatFetching: boolean;
  messages: Array<IMessage>;
  deleteMessage: (message: IMessage) => void;
};

const Content = ({ isChatFetching, messages, deleteMessage }: Props) => {
  let scrollToDiv = useRef<HTMLDivElement>(null);

  const classes = useStyles();
  const myMessage = getBensMessage(messages);
  const tooltips = {
    like: "Double click to toggle like",
    settings: "Double click to delete",
  };

  const editMessageHandler = (message: IMessage) => {
    history.push(`${Paths.MESSAGES}/${message.id}`);
  };

  const removeMessageHandler = (message: IMessage) => {
    if (window.confirm("Are you sure, you want to delete the message?")) {
      deleteMessage(message);
    }
  };

  useEffect(() => {
    if (!isChatFetching && scrollToDiv.current) {
      scrollToDiv.current.scrollIntoView({
        block: "center",
        behavior: "smooth",
      });
    }
  }, [isChatFetching]);

  let content: any = [];
  const groupedMessages = groupByDay(messages);
  if (groupedMessages) {
    content = [];
    const keys = Object.keys(groupedMessages);

    keys.forEach((day) => {
      if (groupedMessages) {
        content.push(<Divider text={day} />);
        groupedMessages[day].forEach((message, index) => {
          content.push(
            <Message
              floatRight={myMessage?.userId === message.userId}
              key={`message-${message.createdAt}`}
              text={message.text}
              avatartUrl={message.avatar}
              time={moment(message.createdAt).format("H:mm")}
              tooltip={
                myMessage?.userId === message.userId
                  ? tooltips.settings
                  : tooltips.like
              }
              removeMessageHandler={() => removeMessageHandler(message)}
              onClick={() => editMessageHandler(message)}
            />
          );
          content.push(<div ref={scrollToDiv}></div>);
        });
      }
    });
  }

  return (
    <Card className={classes.root}>
      <CardContent className={isChatFetching ? classes.textCenter : ""}>
        {isChatFetching && <CircularProgress />}
        {content && !isChatFetching && content.map((item: any) => item)}
      </CardContent>
    </Card>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    messages: state.chat.messages,
    isChatFetching: state.chat.isChatFetching,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    deleteMessage: (message: IMessage) =>
      dispatch(actionCreator.deleteMessage(message)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
