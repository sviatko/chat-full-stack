import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Container } from "@material-ui/core";
import moment from "moment";

import * as actionCreator from "../../store/actions/index";

import Header from "../Header";
import Content from "../Content";
import Controls from "../Controls";

import { IMessage } from "../../interfaces/IMessage";
import { IChatState } from "../../interfaces/IChatState";
import { getTotalUsers } from "../../services/MessageService";

type ChatProps = {
  messages: Array<IMessage>;
  editMessage: (message: IMessage) => void;
  fetchMessages: () => void;
};

const Chat = ({ messages, editMessage, fetchMessages }: ChatProps) => {
  useEffect(() => {
    fetchMessages();
  }, [editMessage, fetchMessages]);

  const messagesTotal = messages.length;
  const usersTotal = getTotalUsers(messages);
  const lastMessageAt =
    messagesTotal > 0
      ? moment(messages[messagesTotal - 1].createdAt).format("H:m")
      : "00:00";

  return (
    <Container maxWidth="md">
      <Header
        title="Chat name"
        messagesTotal={messagesTotal}
        participantsTotal={usersTotal}
        lastMessageAt={lastMessageAt}
      />
      <Content />
      <Controls />
    </Container>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    messages: state.chat.messages,
    isEditing: state.chat.isEditing,
    currentMessage: state.chat.currentMessage,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    editMessage: (message: IMessage) =>
      dispatch(actionCreator.editMessage(message)),
    toggleEditing: () => dispatch(actionCreator.toggleEditing()),
    submitEditing: () => dispatch(actionCreator.submitEditing()),
    setCurrentMessage: (text: string) =>
      dispatch(actionCreator.setCurrentMessage(text)),
    fetchMessages: () => dispatch(actionCreator.fetchMessages()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
