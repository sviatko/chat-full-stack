import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Grid, Card, CardContent } from "@material-ui/core";

import useStyles from "../../assets/jss/components/controlsStyle";
import { IChatState } from "../../interfaces/IChatState";
import * as actionCreator from "../../store/actions";
import InputField from "../InputField";
import Button from "../Button";

type Props = {
  currentMessage: string;
  isEditing: boolean;
  setCurrentMessage: (message: string) => void;
  setMessage: (text: string) => void;
};

const Controls = ({
  currentMessage,
  isEditing,
  setCurrentMessage,
  setMessage,
}: Props) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent>
        <Grid
          container
          justify={"space-between"}
          alignItems={"flex-end"}
          spacing={3}
        >
          <Grid item md={10} sm={10}>
            <InputField
              id={"message"}
              label={"Enter your Message"}
              value={!isEditing ? currentMessage : ""}
              onChange={(e) => setCurrentMessage(e.target.value)}
            />
          </Grid>
          <Grid item md={2} sm={2}>
            <Button
              type={"outlined"}
              title={"Send"}
              onClick={() => setMessage(currentMessage)}
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

const mapStateToProps = (state: { chat: IChatState }) => {
  return {
    currentMessage: state.chat.currentMessage,
    isEditing: state.chat.isEditing,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    setCurrentMessage: (message: string) =>
      dispatch(actionCreator.setCurrentMessage(message)),
    setMessage: (text: string) => dispatch(actionCreator.setMessage(text)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Controls);
