import history from "../../routes/history";
import { IMessage } from "../../interfaces/IMessage";

export const fetchData = (requestUrl: string, options = {}) =>
  fetch(requestUrl, options);

export const forwardTo = (path: string) => history.push(path);

export type Action = {
  type: string;
  payload: { id?: string; message?: IMessage; text?: string };
};

type Options = {
  method?: "PUT" | "POST" | "DELETE";
  headers: {};
  body?: string;
};

export const options: Options = {
  headers: {
    "Content-Type": "application/json",
  },
};
