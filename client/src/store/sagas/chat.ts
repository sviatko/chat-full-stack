import { call, put, takeEvery } from "redux-saga/effects";
import { API_BASE_URL } from "../../config";
import * as actionCreator from "../actions";
import { ActionTypes } from "../actions/actionTypes";
import { IMessage } from "../../interfaces/IMessage";
import { Paths } from "../../routes/paths";
import { generateMessage } from "../../services/MessageService";
import { fetchData, forwardTo, options, Action } from "./common";

function* fetchMessages() {
  const url = API_BASE_URL + Paths.MESSAGES;
  try {
    const messagesResp = yield call(fetchData, url);
    const json = yield messagesResp.json();
    const messages: Array<IMessage> = yield json.data;

    yield put(actionCreator.fetchMessagesSuccess(messages));
  } catch (err) {
    yield put(actionCreator.fetchMessagesFailure());
  }
}

function* fetchMessage(action: Action) {
  const url = `${API_BASE_URL}${Paths.MESSAGES}/${action.payload.id}`;

  try {
    const messageResp = yield call(fetchData, url);
    const json = yield messageResp.json();
    const message: IMessage = yield json.data;

    yield put(actionCreator.fetchMessageByIdSuccess(message));
  } catch (err) {
    yield put(actionCreator.fetchMessageByIdFailure());
  }
}

function* updateMessage(action: Action) {
  if (!action.payload.message) {
    throw new Error("Expect an object type of IMessage");
  }

  const url = `${API_BASE_URL}${Paths.MESSAGES}/${action.payload.message.id}`;
  const messagesUrl = Paths.MESSAGES;

  try {
    options.method = "PUT";

    yield call(fetchData, url, options);
    yield put(actionCreator.updateMessageSuccess());
    yield call(forwardTo, messagesUrl);
  } catch (err) {
    yield put(actionCreator.updateMessageFailure());
  }
}

function* deleteMessage(action: Action) {
  if (!action.payload.message) {
    throw new Error("Expect an object type of IMessage");
  }

  const url = `${API_BASE_URL}${Paths.MESSAGES}/${action.payload.message.id}`;

  try {
    options.method = "DELETE";

    yield call(fetchData, url, options);
    yield put(actionCreator.fetchMessages());
  } catch (err) {
    yield put(actionCreator.deleteMessageFailure());
  }
}

function* setMessage(action: Action) {
  if (!action.payload.text) {
    throw new Error("Expect a string");
  }

  const url = `${API_BASE_URL}${Paths.MESSAGES}`;

  try {
    const data: string = action.payload.text;
    const msg = generateMessage(data);

    options.method = "POST";
    options.body = JSON.stringify({ ...msg });

    yield call(fetchData, url, options);

    yield put(actionCreator.fetchMessages());
  } catch (err) {
    yield put(actionCreator.setMessageFailure());
  }
}

export function* watchFetchMessages() {
  yield takeEvery(ActionTypes.FETCH_MESSAGES, fetchMessages);
  yield takeEvery(ActionTypes.FETCH_MESSAGE_BY_ID, fetchMessage);
  yield takeEvery(ActionTypes.UPDATE_MESSAGE, updateMessage);
  yield takeEvery(ActionTypes.DELETE_MESSAGE, deleteMessage);
  yield takeEvery(ActionTypes.SET_MESSAGE, setMessage);
}
