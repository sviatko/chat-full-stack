import { all } from "redux-saga/effects";

import { watchFetchMessages } from "./chat";
import { watchFetchUsers } from "./users";
import { watchFetchAuth } from "./auth";

export default function* rootSaga() {
  yield all([watchFetchMessages(), watchFetchUsers(), watchFetchAuth()]);
}
