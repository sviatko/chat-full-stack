import { call, put, takeEvery } from "redux-saga/effects";

import { Paths } from "../../routes/paths";
import { fetchData, forwardTo, options } from "./common";
import * as actionCreator from "../actions";
import { API_BASE_URL } from "../../config";
import { ActionTypes } from "../actions/actionTypes";
import { IUser } from "../../interfaces/IUser";

type Action = { type: "string"; payload: { login: string; password: string } };
function* login(action: Action) {
  const url = API_BASE_URL + Paths.LOGIN;

  options.method = "POST";
  options.body = JSON.stringify({
    email: action.payload.login,
    password: action.payload.password,
  });
  try {
    if (action.payload.login === "" || action.payload.password === "") {
      throw new Error("Fill all credentials");
    }
    const user: IUser = yield fetchData(url, options)
      .then((resp) => resp.json())
      .then((data) => data.data)
      .catch((err) => {
        throw new Error("Wrong credentials");
      });

    yield put(actionCreator.authSuccess(user.role));

    let redirect = Paths.MESSAGES;

    if (user.role === "admin") {
      redirect = Paths.USERS;
    }

    yield call(forwardTo, redirect);
  } catch (err) {
    yield put(actionCreator.authFailure(err.message));
  }
}

export function* watchFetchAuth() {
  yield takeEvery(ActionTypes.LOGIN, login);
}
