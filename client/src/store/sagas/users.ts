import { call, put, takeEvery } from "redux-saga/effects";
import { ActionTypes } from "../actions/actionTypes";
import { API_BASE_URL } from "../../config";
import { Paths } from "../../routes/paths";
import * as actionCreator from "../actions";
import { fetchData } from "./common";
import { IUser } from "../../interfaces/IUser";
import { options, forwardTo } from "./common";

type Action = {
  type: string;
  payload: { id?: string; user: IUser; text?: string };
};

function* fetchUsers() {
  const url = `${API_BASE_URL}${Paths.USERS}`;

  try {
    const users: Array<IUser> = yield fetchData(url)
      .then((resp) => resp.json())
      .then((data) => data.data);

    yield put(actionCreator.fetchUsersSuccess(users));
  } catch (er) {
    yield put(actionCreator.fetchUsersFailure());
  }
}

function* fetchUserById(action: Action) {
  const url = `${API_BASE_URL}${Paths.USERS}/${action.payload.id}`;

  try {
    const user: IUser = yield fetchData(url)
      .then((resp) => resp.json())
      .then((data) => data.data);

    yield put(actionCreator.fetchUserByIdSuccess(user));
  } catch (er) {
    yield put(actionCreator.fetchUserByIdFailure());
  }
}

function* deleteUser(action: Action) {
  const url = `${API_BASE_URL}${Paths.USERS}/${action.payload.id}`;
  options.method = "DELETE";

  try {
    yield call(fetchData, url, options);
    yield put(actionCreator.deleteUserSuccess());
    yield put(actionCreator.fetchUsers());
  } catch (er) {
    yield put(actionCreator.deleteUserFailure());
  }
}

function* createUser(action: Action) {
  const url = `${API_BASE_URL}${Paths.USERS}`;
  options.method = "POST";
  options.body = JSON.stringify({ ...action.payload.user });

  try {
    yield call(fetchData, url, options);
    yield put(actionCreator.createUserSuccess());
    yield call(forwardTo, Paths.USERS);
  } catch (er) {
    yield put(actionCreator.createUserFailure(er));
  }
}

function* updateUser(action: Action) {
  const url = `${API_BASE_URL}${Paths.USERS}/${action.payload.user.id}`;
  options.method = "PUT";
  options.body = JSON.stringify({ ...action.payload.user });

  try {
    yield call(fetchData, url, options);
    yield put(actionCreator.updateCurrentUserSuccess());
    yield call(forwardTo, Paths.USERS);
  } catch (er) {
    yield put(actionCreator.updateCurrentUserFailure(er));
  }
}

export function* watchFetchUsers() {
  yield takeEvery(ActionTypes.FETCH_USERS, fetchUsers);
  yield takeEvery(ActionTypes.FETCH_USER_BY_ID, fetchUserById);
  yield takeEvery(ActionTypes.DELETE_USER, deleteUser);
  yield takeEvery(ActionTypes.CREATE_CURRENT_USER, createUser);
  yield takeEvery(ActionTypes.UPDATE_CURRENT_USER, updateUser);
}
