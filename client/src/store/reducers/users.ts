import { IUser } from "../../interfaces/IUser";
import { IUserState as State } from "../../interfaces/IUserState";
import { ActionTypes } from "../actions/actionTypes";

type Action = {
  type: string;
  payload: {
    users?: Array<IUser>;
    currentUser?: IUser;
    value: string;
    field: string;
    error: string;
  };
};

const initialState = {
  users: [],
  currentUser: null,
  isUsersFetching: false,
  error: null,
};

const reducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case ActionTypes.FETCH_USERS:
      return fetchUsers(state);
    case ActionTypes.FETCH_USERS_SUCCESS:
      return fetchUsersSuccess(state, action);
    case ActionTypes.FETCH_USERS_FAILURE:
      return fetchUsersFailure(state);
    case ActionTypes.FETCH_USER_BY_ID:
      return fetchUserById(state);
    case ActionTypes.FETCH_USER_BY_ID_SUCCESS:
      return fetchUserByIdSuccess(state, action);
    case ActionTypes.FETCH_USER_BY_ID_FAILURE:
      return fetchUserByIdFailure(state);
    case ActionTypes.EDIT_CURRENT_USER:
      return editCurrentUser(state, action);
    case ActionTypes.DELETE_USER:
      return deleteUser(state);
    case ActionTypes.DELETE_USER_SUCCESS:
      return deleteUserSuccess(state);
    case ActionTypes.DELETE_USER_FAILURE:
      return deleteUserFailure(state);
    case ActionTypes.UPDATE_CURRENT_USER:
      return updateCurrentUser(state);
    case ActionTypes.UPDATE_CURRENT_USER_SUCCESS:
      return updateCurrentUserSuccess(state);
    case ActionTypes.UPDATE_CURRENT_USER_FAILURE:
      return updateCurrentUserFailure(state, action);
    case ActionTypes.CREATE_CURRENT_USER:
      return createUser(state);
    case ActionTypes.CREATE_CURRENT_USER_SUCCESS:
      return createUserSuccess(state);
    case ActionTypes.CREATE_CURRENT_USER_FAILURE:
      return createUserFailure(state, action);
    default:
      return state;
  }
};

const fetchUsers = (state: State) => {
  return {
    ...state,
    isUsersFetching: true,
  };
};

const fetchUsersSuccess = (state: State, action: Action) => {
  return {
    ...state,
    isUsersFetching: false,
    users: action.payload.users,
  };
};

const fetchUsersFailure = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
  };
};

const fetchUserById = (state: State) => {
  return {
    ...state,
    isUsersFetching: true,
  };
};

const fetchUserByIdSuccess = (state: State, action: Action) => {
  return {
    ...state,
    isUsersFetching: false,
    currentUser: action.payload.currentUser,
  };
};

const fetchUserByIdFailure = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
  };
};

const editCurrentUser = (state: State, action: Action) => {
  return {
    ...state,
    currentUser: {
      ...state.currentUser,
      [action.payload.field]: action.payload.value,
    },
  };
};

const deleteUser = (state: State) => {
  return {
    ...state,
    isUsersFetching: true,
  };
};

const deleteUserSuccess = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
  };
};

const deleteUserFailure = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
  };
};

const updateCurrentUser = (state: State) => {
  return {
    ...state,
    isUsersFetching: true,
  };
};

const updateCurrentUserSuccess = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
    error: null,
  };
};

const updateCurrentUserFailure = (state: State, action: Action) => {
  return {
    ...state,
    isUsersFetching: false,
    error: action.payload.error,
  };
};

const createUser = (state: State) => {
  return {
    ...state,
    isUsersFetching: true,
  };
};

const createUserSuccess = (state: State) => {
  return {
    ...state,
    isUsersFetching: false,
    currentUser: null,
    error: null,
  };
};

const createUserFailure = (state: State, action: Action) => {
  return {
    ...state,
    isUsersFetching: false,
    currentUser: null,
    error: action.payload.error,
  };
};

export default reducer;
