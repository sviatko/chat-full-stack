import { IAuthState } from "../../interfaces/IAuthState";
import { ActionTypes } from "../actions/actionTypes";

type Action = {
  type: string;
  payload: {
    email: string;
    password: string;
    role: string;
    error?: string;
    field: "password" | "email";
    value: string;
  };
};

const initState = {
  data: {
    email: "",
    password: "",
    role: "",
  },
  error: null,
  isFetching: false,
};

const reducer = (state: IAuthState = initState, action: Action) => {
  switch (action.type) {
    case ActionTypes.LOGIN:
      return login(state, action);
    case ActionTypes.LOGIN_CHANGE:
      return loginChange(state, action);
    case ActionTypes.LOGIN_SUCCESS:
      return loginSuccess(state, action);
    case ActionTypes.LOGIN_FAILURE:
      return loginFailure(state, action);
    default:
      return state;
  }
};

const login = (state: IAuthState, action: Action): IAuthState => {
  return {
    ...state,
    isFetching: true,
    data: {
      email: action.payload.email,
      password: action.payload.password,
      role: "",
    },
    error: null,
  };
};

const loginChange = (state: IAuthState, action: Action): IAuthState => {
  const upd = { ...state.data };
  upd[action.payload.field] = action.payload.value;

  return {
    ...state,
    data: {
      ...upd,
    },
  };
};

const loginSuccess = (state: IAuthState, action: Action): IAuthState => {
  return {
    ...state,
    isFetching: false,
    data: {
      email: "",
      password: "",
      role: action.payload.role,
    },
    error: null,
  };
};

const loginFailure = (state: IAuthState, action: Action): IAuthState => {
  if (!action.payload.error) {
    throw new Error("Expect for error message");
  }
  return {
    ...state,
    isFetching: false,
    data: {
      email: "",
      password: "",
      role: "",
    },
    error: action.payload.error,
  };
};

export default reducer;
