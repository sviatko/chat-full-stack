import { IMessage } from "../../interfaces/IMessage";
import { IChatState } from "../../interfaces/IChatState";
import { ActionTypes } from "../actions/actionTypes";
import { sortMessages } from "../../services/MessageService";

type Action = {
  type: string;
  payload: {
    messages?: Array<IMessage>;
    message?: IMessage;
    text?: string;
    id?: string;
  };
};

const initialState: IChatState = {
  messages: [],
  currentMessage: "",
  editingMessage: null,
  isEditing: false,
  isChatFetching: false,
};

const reducer = (state: IChatState = initialState, action: Action) => {
  switch (action.type) {
    case ActionTypes.FETCH_MESSAGES:
      return fetchMessages(state);
    case ActionTypes.FETCH_MESSAGES_SUCCESS:
      return fetchMessagesSuccess(state, action);
    case ActionTypes.FETCH_MESSAGE_BY_ID_SUCCESS:
      return fetchMessageByIdSuccess(state, action);
    case ActionTypes.UPDATE_MESSAGE:
      return updateMessage(state, action);
    case ActionTypes.DELETE_MESSAGE:
      return deleteMessage(state);
    case ActionTypes.DELETE_MESSAGE_SUCCESS:
      return deleteMessageSuccess(state);
    case ActionTypes.DELETE_MESSAGE_FAILURE:
      return deleteMessageFailure(state);
    // case ActionTypes.EDIT_MESSAGE:
    //   return editMessage(state, action);
    case ActionTypes.SET_MESSAGE:
      return setMessage(state);
    case ActionTypes.SET_MESSAGE_SUCCESS:
      return setMessageSuccess(state, action);
    case ActionTypes.SET_MESSAGE_FAILURE:
      return setMessageFailure(state);
    // case ActionTypes.DELETE_MESSAGE:
    //   return removeMessage(state, action);
    // case ActionTypes.TOGGLE_EDITING:
    //   return toggleEditing(state);
    // case ActionTypes.SUBMIT_EDITING:
    // return submitEditing(state);
    case ActionTypes.SET_CURRENT_MESSAGE:
      return setCurrentMessage(state, action);
    default:
      return state;
  }
};

const fetchMessages = (state: IChatState): IChatState => {
  return { ...state, isChatFetching: true };
};

const fetchMessagesSuccess = (
  state: IChatState,
  action: Action
): IChatState => {
  if (!action.payload.messages) {
    throw new Error("Expected to get an array of IMessages");
  }

  const messages = sortMessages([...action.payload.messages]);

  return { ...state, messages: messages, isChatFetching: false };
};

const fetchMessageByIdSuccess = (
  state: IChatState,
  action: Action
): IChatState => {
  if (!action.payload.message) {
    throw new Error("Expected to get an object of IMessages");
  }
  const message = action.payload.message;

  return {
    ...state,
    currentMessage: message.text,
    editingMessage: message,
  };
};

const updateMessage = (state: IChatState, action: Action): IChatState => {
  if (!action.payload.message) {
    throw new Error("Expected to get an object of type IMessage");
  }

  return {
    ...state,
    currentMessage: "",
    editingMessage: null,
  };
};

const deleteMessage = (state: IChatState): IChatState => {
  return {
    ...state,
    isChatFetching: true,
  };
};

const deleteMessageSuccess = (state: IChatState): IChatState => {
  return {
    ...state,
    isChatFetching: false,
  };
};

const deleteMessageFailure = (state: IChatState): IChatState => {
  return {
    ...state,
    isChatFetching: false,
  };
};

const setMessage = (state: IChatState) => {
  return {
    ...state,
    isFetching: true,
    currentMessage: "",
  };
};

const setMessageSuccess = (state: IChatState, action: Action) => {
  if (!action.payload.messages) {
    throw new Error("Expected to get an array of IMessages");
  }

  return {
    ...state,
    isFetching: false,
    messages: action.payload.messages,
  };
};

const setMessageFailure = (state: IChatState) => {
  return {
    ...state,
    isFetching: false,
  };
};

const setCurrentMessage = (state: IChatState, action: Action): IChatState => {
  if (!action.payload.text) {
    throw new Error("Expected to get an IMessage object");
  }
  return {
    ...state,
    currentMessage: action.payload.text,
  };
};

export default reducer;
