import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import chatReducer from "./reducers/chat";
import userReducer from "./reducers/users";
import authReducer from "./reducers/auth";
import rootSaga from "./sagas";

const rootReducer = combineReducers({
  chat: chatReducer,
  user: userReducer,
  auth: authReducer,
});

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

export default store;
