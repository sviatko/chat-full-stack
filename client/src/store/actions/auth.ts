import { ActionTypes } from "./actionTypes";

export const auth = (login: string, password: string) => {
  return {
    type: ActionTypes.LOGIN,
    payload: {
      login,
      password,
    },
  };
};

export const authChange = (field: string, value: string) => {
  return {
    type: ActionTypes.LOGIN_CHANGE,
    payload: {
      field,
      value,
    },
  };
};

export const authSuccess = (role: string) => {
  return {
    type: ActionTypes.LOGIN_SUCCESS,
    payload: {
      role,
    },
  };
};

export const authFailure = (error: string) => {
  return {
    type: ActionTypes.LOGIN_FAILURE,
    payload: {
      error,
    },
  };
};
