import { ActionTypes } from "./actionTypes";
import { IUser } from "../../interfaces/IUser";

export const fetchUsers = () => {
  return {
    type: ActionTypes.FETCH_USERS,
  };
};

export const fetchUsersSuccess = (users: Array<IUser>) => {
  return {
    type: ActionTypes.FETCH_USERS_SUCCESS,
    payload: {
      users,
    },
  };
};

export const fetchUsersFailure = () => {
  return {
    type: ActionTypes.FETCH_USERS_FAILURE,
  };
};

export const fetchUserById = (id: string) => {
  return {
    type: ActionTypes.FETCH_USER_BY_ID,
    payload: {
      id,
    },
  };
};

export const fetchUserByIdSuccess = (user: IUser) => {
  return {
    type: ActionTypes.FETCH_USER_BY_ID_SUCCESS,
    payload: {
      currentUser: user,
    },
  };
};

export const fetchUserByIdFailure = () => {
  return {
    type: ActionTypes.FETCH_USER_BY_ID_FAILURE,
  };
};

export const editCurrentUser = (value: string, field: string) => {
  return {
    type: ActionTypes.EDIT_CURRENT_USER,
    payload: {
      value,
      field,
    },
  };
};

export const updateCurrentUser = (user: IUser) => {
  return {
    type: ActionTypes.UPDATE_CURRENT_USER,
    payload: {
      user,
    },
  };
};

export const updateCurrentUserSuccess = () => {
  return {
    type: ActionTypes.UPDATE_CURRENT_USER_SUCCESS,
  };
};

export const updateCurrentUserFailure = (error: string) => {
  return {
    type: ActionTypes.UPDATE_CURRENT_USER_FAILURE,
    payload: { error },
  };
};

export const deleteUser = (id: string) => {
  return {
    type: ActionTypes.DELETE_USER,
    payload: {
      id,
    },
  };
};

export const deleteUserSuccess = () => {
  return {
    type: ActionTypes.DELETE_USER_SUCCESS,
  };
};

export const deleteUserFailure = () => {
  return {
    type: ActionTypes.DELETE_USER_FAILURE,
  };
};

export const createUser = (user: IUser) => {
  return {
    type: ActionTypes.CREATE_CURRENT_USER,
    payload: {
      user,
    },
  };
};

export const createUserSuccess = () => {
  return {
    type: ActionTypes.CREATE_CURRENT_USER_SUCCESS,
  };
};

export const createUserFailure = (error: string) => {
  return {
    type: ActionTypes.CREATE_CURRENT_USER_FAILURE,
    payload: {
      error,
    },
  };
};
