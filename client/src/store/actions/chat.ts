import { ActionTypes } from "./actionTypes";
import { IMessage } from "../../interfaces/IMessage";

export const fetchMessages = () => {
  return {
    type: ActionTypes.FETCH_MESSAGES,
  };
};

export const fetchMessagesSuccess = (messages: Array<IMessage>) => {
  return {
    type: ActionTypes.FETCH_MESSAGES_SUCCESS,
    payload: {
      messages,
    },
  };
};

export const fetchMessagesFailure = () => {
  return {
    type: ActionTypes.FETCH_MESSAGES_FAILURE,
  };
};

export const fetchMessageById = (id: string) => {
  return {
    type: ActionTypes.FETCH_MESSAGE_BY_ID,
    payload: {
      id,
    },
  };
};

export const fetchMessageByIdSuccess = (message: IMessage) => {
  return {
    type: ActionTypes.FETCH_MESSAGE_BY_ID_SUCCESS,
    payload: {
      message,
    },
  };
};

export const fetchMessageByIdFailure = () => {
  return {
    type: ActionTypes.FETCH_MESSAGE_BY_ID_FAILURE,
  };
};

export const updateMessage = (message: IMessage) => {
  return {
    type: ActionTypes.UPDATE_MESSAGE,
    payload: {
      message,
    },
  };
};

export const updateMessageSuccess = () => {
  return {
    type: ActionTypes.UPDATE_MESSAGE_SUCCESS,
  };
};

export const updateMessageFailure = () => {
  return {
    type: ActionTypes.UPDATE_MESSAGE_FAILURE,
  };
};

export const deleteMessage = (message: IMessage) => {
  return {
    type: ActionTypes.DELETE_MESSAGE,
    payload: { message },
  };
};

export const deleteMessageSuccess = () => {
  return {
    type: ActionTypes.DELETE_MESSAGE_SUCCESS,
  };
};

export const deleteMessageFailure = () => {
  return {
    type: ActionTypes.DELETE_MESSAGE_FAILURE,
  };
};

export const setMessage = (text: string) => {
  return {
    type: ActionTypes.SET_MESSAGE,
    payload: {
      text,
    },
  };
};

export const setMessageSuccess = (message: IMessage) => {
  return {
    type: ActionTypes.SET_MESSAGE_SUCCESS,
    payload: { message },
  };
};

export const setMessageFailure = () => {
  return {
    type: ActionTypes.SET_MESSAGE_FAILURE,
  };
};

///////
export const storeMessage = () => {
  return {
    type: ActionTypes.STORE_MESSAGE,
  };
};

export const editMessage = (message: IMessage) => {
  return {
    type: ActionTypes.EDIT_MESSAGE,
    payload: { message },
  };
};

export const toggleEditing = () => {
  return {
    type: ActionTypes.TOGGLE_EDITING,
  };
};

export const submitEditing = () => {
  return {
    type: ActionTypes.SUBMIT_EDITING,
  };
};

export const setCurrentMessage = (message: string) => {
  return {
    type: ActionTypes.SET_CURRENT_MESSAGE,
    payload: { text: message },
  };
};
