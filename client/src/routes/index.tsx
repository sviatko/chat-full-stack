import React from "react";
import { Router, Switch, Route } from "react-router-dom";

import Login from "../containers/Login";
import Messages from "../containers/Messages";
import Message from "../containers/Message";
import Users from "../containers/Users";
import User from "../containers/User";
import { Paths } from "./paths";
import history from "./history";

const routes = () => (
  <Router history={history}>
    <Switch>
      <Route exact path={Paths.HOME} component={Login} />
      <Route path={Paths.MESSAGE} component={Message} />
      <Route path={Paths.MESSAGES} component={Messages} />
      <Route path={Paths.USER} component={User} />
      <Route path={Paths.USERS} component={Users} />
      <Route path={Paths.USERS_CREATE} component={User} />
    </Switch>
  </Router>
);

export default routes;
