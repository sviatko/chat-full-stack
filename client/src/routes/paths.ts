export enum Paths {
  HOME = "/",
  MESSAGES = "/messages",
  MESSAGE = "/messages/:id",
  USERS = "/users",
  USERS_CREATE = "/users/create",
  USER = "/users/:id",
  LOGIN = "/auth/login",
}
