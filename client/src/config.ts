export const API_BASE_URL = "http://localhost:3001/api";
export const FETCH_MESSAGE_URL =
  "https://edikdolynskyi.github.io/react_sources/messages.json";

export const USER_ID = "5328dba1-1b8f-11e8-9629-c7eca82aa7bd";
export const USER_NAME = "Ben";
export const USER_AVATAR =
  "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg";
