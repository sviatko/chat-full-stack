// Colors
const gray = ["#e0e0e0", "#ababab", "#484848"];
const primary = ["#fec713", "#0091c7"];
const secondary = ["#fec713", "#826200", "#423200", "#ad8500", "#2b2100"];
const white = "#fff";

const transition = {
  transition: "all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)",
};

const boxShadow = ["1px 1px 2px"];

const container = {
  paddingRight: "15px",
  paddingLeft: "15px",
  marginRight: "auto",
  marginLeft: "auto",
};

const defaultFont = {
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  fontWeight: "300",
  lineHeight: "1.5em",
};

const defaultPadding = ["12px 24px"];

export {
  defaultFont,
  transition,
  container,
  gray,
  boxShadow,
  defaultPadding,
  secondary,
  primary,
  white,
};
