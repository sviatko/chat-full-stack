import { makeStyles } from "@material-ui/core/styles";

const userStyle = makeStyles({
  gridPadding: {
    padding: "0 12px 0 0",
  },
  root: {
    marginTop: "2em",
    display: "flex",
    "& button": {
      marginLeft: "1em",
    },
  },
});

export default userStyle;
