import { makeStyles } from "@material-ui/core/styles";

const messageStyle = makeStyles({
  root: {
    marginTop: "2em",
    display: "flex",
    "& button": {
      marginLeft: "1em",
    },
  },
});

export default messageStyle;
