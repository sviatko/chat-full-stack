import { makeStyles } from "@material-ui/core/styles";
import { primary, gray } from "..";

const controlsStyle = {
  root: {
    // marginRight: "1em",
  },
  textSecondary: {
    color: "red",
  },
  outlinedPrimary: {
    color: `${primary[1]} !important`,
    borderColor: "rgb(0,145,199, 0.5) !important",
  },
  textPrimary: {
    color: `${gray[1]} !important`,
    fontSize: "12px",
  },
};

export default makeStyles(controlsStyle);
