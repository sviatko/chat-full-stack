import { secondary } from "..";

import { makeStyles } from "@material-ui/core/styles";

const dividerStyles = {
  root: {
    textAlign: "center",
    marginBottom: "42px",
    position: "sticky",
    top: "12px",
    zIndex: 99,
  },
  text: {
    padding: "1px 24px",
    backgroundColor: secondary[3],
    borderRadius: "50px",
    fontSize: "12px",
    color: secondary[2],
    fontStyle: "italic",
  },
};

export default makeStyles(dividerStyles);
