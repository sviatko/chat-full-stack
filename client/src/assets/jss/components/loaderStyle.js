import { makeStyles } from "@material-ui/core/styles";

const leaderStyle = makeStyles({
  root: {
    position: "absolute",
    top: "50%",
    left: "50%",
  },
});

export default leaderStyle;
