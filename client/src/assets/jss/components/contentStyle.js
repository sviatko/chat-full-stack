import { makeStyles } from "@material-ui/core/styles";
import { defaultPadding } from "..";

const contentStyles = {
  root: {
    marginTop: "1.5em",
    padding: defaultPadding[0],
    maxHeight: "400px",
    overflowY: "scroll",
  },

  textCenter: {
    textAlign: "center",
  },
};

export default makeStyles(contentStyles);
