import { makeStyles } from "@material-ui/core/styles";

const controlsStyle = {
  root: {
    marginTop: "1.5em",
  },
};

export default makeStyles(controlsStyle);
