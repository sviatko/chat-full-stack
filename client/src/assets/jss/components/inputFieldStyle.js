import { makeStyles } from "@material-ui/core/styles";
import { primary } from "..";

const controlsStyle = {
  underline: {
    "&:after": {
      borderBottom: `2px solid ${primary[1]}`,
    },
  },
  focused: {
    color: `${primary[1]} !important`,
  },
};

export default makeStyles(controlsStyle);
