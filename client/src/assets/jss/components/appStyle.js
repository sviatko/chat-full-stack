import { makeStyles } from "@material-ui/core/styles";

import { gray, defaultFont } from "../index";

const useStyles = makeStyles({
  root: {
    height: "100%",
    backgroundColor: gray[0],
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
    color: gray[2],
    ...defaultFont,
  },
});

export default useStyles;
