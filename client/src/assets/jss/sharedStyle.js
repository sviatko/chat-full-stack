import { makeStyles } from "@material-ui/core/styles";

const sharedStyles = makeStyles({
  textCenter: {
    textAlign: "center",
  },
  relative: {
    position: "relative",
  },
  mt: {
    marginTop: "1em",
  },
  error: {
    color: "red",
  },
});

export default sharedStyles;
