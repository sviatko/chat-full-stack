import { IMessage } from "./IMessage";

export interface IChatState {
  messages: Array<IMessage>;
  currentMessage: string;
  editingMessage: IMessage | null;
  isEditing: boolean;
  isChatFetching: boolean;
}
