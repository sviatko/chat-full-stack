export interface IAuthState {
  data: {
    email: string;
    password: string;
    role: string;
    [x: string]: string;
  };
  error: string | null;
  isFetching: boolean;
}
