import { IUser } from "./IUser";

export interface IUserState {
  users: Array<IUser>;
  currentUser: IUser | null;
  isUsersFetching: boolean;
  error: string | null;
}
