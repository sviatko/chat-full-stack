export interface IMessage {
  readonly id?: string;
  text: string;
  avatar: string;
  createdAt: string;
  editedAt?: string;
  user: string;
  userId: string;
  liked?: boolean;
}
