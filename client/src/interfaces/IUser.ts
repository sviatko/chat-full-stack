export interface IUser {
  readonly id: string;
  username: string;
  email: string;
  password: string;
  role: string;
}
